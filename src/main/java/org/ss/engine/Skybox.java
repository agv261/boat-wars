package org.ss.engine;

import org.ss.engine.graph.Material;
import org.ss.engine.graph.Mesh;
import org.ss.engine.graph.Texture;

public class Skybox extends GameItem {

    public Skybox(Mesh mesh, String textureFile) throws Exception {
        super(mesh);
        Mesh skyBoxMesh = mesh;
        Texture skyBoxtexture = new Texture(textureFile);
        skyBoxMesh.setMaterial(new Material(skyBoxtexture, 0.0f));
        setMesh(skyBoxMesh);
        setPosition(0, 13, 0);
    }
}
