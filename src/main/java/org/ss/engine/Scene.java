package org.ss.engine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.ss.engine.graph.Mesh;

public class Scene {

    private Map<Mesh, List<GameItem>> meshMap;

    public GameItem[] gameItems;

    private List<int[]> obstacleCoords;

    private Skybox skybox;

    private SceneLight sceneLight;

    public Scene() {

        meshMap = new HashMap();
        int[] xs = new int[]{47,20,75,47,15,80,47,20,75,47,15,80};
        int[] zs = new int[]{-25,-20,-20,-15,-10,-10,-45,-55,-55,-60,-65,-65};
        int[] scales = new int[]{4,3,3,2,2,2,4,3,3,2,2,2};

        obstacleCoords = new ArrayList<>();
        obstacleCoords.add(xs);
        obstacleCoords.add(zs);
        obstacleCoords.add(scales);

    }

    public Map<Mesh, List<GameItem>> getGameMeshes() {
        return meshMap;
    }

    public void setGameItems(GameItem[] gameItems) {
        this.gameItems = gameItems;
        int numGameItems = gameItems != null ? gameItems.length : 0;
        for (int i=0; i<numGameItems; i++) {
            GameItem gameItem = gameItems[i];
            Mesh mesh = gameItem.getMesh();
            List<GameItem> list = meshMap.get(mesh);
            if ( list == null ) {
                list = new ArrayList<>();
                meshMap.put(mesh, list);
            }
            list.add(gameItem);
        }
    }
    public GameItem[] getgameItems(){
        return this.gameItems;
    }

    public List<int[]> getObstaclesCoords() {
        return this.obstacleCoords;
    }
    public Skybox getSkybox() {
        return skybox;
    }

    public void setSkybox(Skybox skyBox) {
        this.skybox = skyBox;
    }

    public SceneLight getSceneLight() {
        return sceneLight;
    }

    public void setSceneLight(SceneLight sceneLight) {
        this.sceneLight = sceneLight;
    }

}
