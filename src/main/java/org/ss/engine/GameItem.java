package org.ss.engine;

import org.joml.Vector3f;
import org.ss.engine.graph.Mesh;
import org.ss.engine.graph.StaticItem;

public class GameItem extends StaticItem {

    private Mesh[] meshes;

    private Vector3f position;

    private float scale;

    private final Vector3f rotation;

    private Vector3f velocity;

    public GameItem(Mesh mesh) {
        super(mesh);
        this.meshes = new Mesh[]{mesh};
        position = new Vector3f();
        scale = 1;
        rotation = new Vector3f();
        velocity = new Vector3f();
    }

    public GameItem(Mesh[] meshes) {
        super(meshes);
        this.meshes = meshes;
        position = new Vector3f();
        scale = 1;
        rotation = new Vector3f();
        velocity = new Vector3f();
    }

    public Vector3f getPosition() {
        return position;
    }

    public void setPosition(float x, float y, float z) {
        this.position.x = x;
        this.position.y = y;
        this.position.z = z;
    }

    public void movePosition(float offsetX, float offsetY, float offsetZ) {
        if (offsetZ != 0) {
            position.x += (float) Math.sin(Math.toRadians(rotation.y)) * -1.0f * offsetZ;
            position.z += (float) Math.cos(Math.toRadians(rotation.y)) * offsetZ;
        }
        if (offsetX != 0) {
            position.x += (float) Math.sin(Math.toRadians(rotation.y - 90)) * -1.0 * offsetX;
            position.z += (float) Math.cos(Math.toRadians(rotation.y - 90)) * offsetX;
        }
        position.y += offsetY;
    }

    public void movePosition(Vector3f velocity) {
        if (velocity.z != 0) {
            position.x += (float) Math.sin(Math.toRadians(rotation.y)) * -1.0f * velocity.z;
            position.z += (float) Math.cos(Math.toRadians(rotation.y)) * velocity.z;
        }
        if (velocity.x != 0) {
            position.x += (float) Math.sin(Math.toRadians(rotation.y - 90)) * -1.0 * velocity.x;
            position.z += (float) Math.cos(Math.toRadians(rotation.y - 90)) * velocity.x;
        }
        position.y += velocity.y;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    public Vector3f getRotation() {
        return rotation;
    }

    public void setRotation(float x, float y, float z) {
        this.rotation.x = x;
        this.rotation.y = y;
        this.rotation.z = z;
    }

    public Vector3f getVelocity() {
        return velocity;
    }

    public void setVelocity(float x, float y, float z) {
        this.velocity.x = x;
        this.velocity.y = y;
        this.velocity.z = z;
    }

    public Mesh getMesh() {
        return meshes[0];
    }

    public Mesh[] getMeshes() {
        return meshes;
    }

    public void setMeshes(Mesh[] meshes) {
        this.meshes = meshes;
    }

    public void setMesh(Mesh mesh){
        this.meshes = new Mesh[]{mesh};
    }

    public void setVelocity(Vector3f velocity) {
        this.velocity = velocity;
    }

    public void setPosition(Vector3f position) {
        this.position = position;
    }
}

