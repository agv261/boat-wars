package org.ss.engine;

import org.joml.Vector3f;

import org.ss.engine.graph.Mesh;
import org.ss.engine.graph.OBJLoader;
import org.ss.game.object.Player;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GameItemLoader {
    public static GameItem[] spawnGameItem(GameItem[] gameItems, Mesh mesh, float scale, float x, float y, float z,
                                           float dX, float dY, float dZ) {
        GameItem gameItem = new GameItem(mesh);
        gameItem.setScale(scale);
        gameItem.setPosition(x, y, z);
        gameItem.setVelocity(dX, dY, dZ);
        return insertGameItem(gameItems, gameItem);
    }

    public static GameItem[] spawnActor(GameItem[] gameItems, Mesh mesh, float scale, float x, float y, float z,
                                        float dX, float dY, float dZ){
        Player gameItem = new Player(mesh);
        gameItem.setScale(scale);
        gameItem.setPosition(x, y, z);
        gameItem.setVelocity(dX, dY, dZ);
        return insertGameItem(gameItems, gameItem);
    }

    public static ArrayList<Mesh> loadAnimation(String animationName) throws Exception {
        ArrayList<Mesh> meshList = new ArrayList<>();
        File dir = new File("src/main/resources/models/"+animationName+"/");
        for (String filename : dir.list()) {
            meshList.add(OBJLoader.loadMesh("/models/"+animationName+"/"+filename));
        }
        return meshList;

    }

    public static GameItem[] spawnNItemsInArea(GameItem[] gameItems, Mesh mesh, int num_items,
                                               float scale, int xAreaDim, int yAreaDim, int zAreaDim) {
        int x, y, z;
        boolean posConflict;

        for (int i = 0; i < num_items; i++) {
            do {
                posConflict = false;

                x = Utils.randInt(-1 * xAreaDim / 2, xAreaDim / 2);
                y = Utils.randInt(-1 * yAreaDim / 2, yAreaDim / 2);
                z = 0;

                for (GameItem gameItem: gameItems) {
                    Vector3f position = gameItem.getPosition();
                    if ((position.x == x) && (position.y == y) && (position.z == z)) {
                        posConflict = true;
                        break;
                    }
                }
            } while (posConflict);
            gameItems = spawnGameItem(gameItems, mesh, scale, x, y, z,0,0,0);
        }

        return gameItems;
    }

    public static GameItem[] spawnObstacles(GameItem[] gameItems, Mesh mesh, List<int[]> coords) {
        int[] xs = coords.get(0);
        int[] zs = coords.get(1);
        int[] scales = coords.get(2);
        for (int i=0; i < xs.length; i++){
            gameItems = spawnGameItem(gameItems, mesh, scales[i], xs[i], 0, zs[i],0,0,0);
        }
        return gameItems;
    }

    public static GameItem[] spawnNItemsAtPosition(GameItem[] gameItems, Mesh mesh, int num_items,
                                                   float scale, Vector3f position) {
        float x, y, z;
        float dX, dY, dZ;

        for (int i = 0; i < num_items; i++) {
            x = position.x;
            y = position.y;
            z = position.z;

            dX = Utils.randInt(4,8)*Utils.randFloat(-1.0f, 1.0f);
            dY = Utils.randInt(4,8)*Utils.randFloat(-1.0f, 1.0f);
            dZ = Utils.randInt(4,8)*Utils.randFloat(-1.0f, 1.0f);

            gameItems = spawnGameItem(gameItems, mesh, scale, x-2, y-1, z, dX, dY, dZ);
        }

        return gameItems;
    }

    public static GameItem[] insertGameItem(GameItem[] gameItems, GameItem gameItem) {
        GameItem[] newGameItems = new GameItem[gameItems.length + 1];
        System.arraycopy(gameItems, 0, newGameItems, 0, gameItems.length);
        newGameItems[gameItems.length] = gameItem;
        return newGameItems;
    }

    public static GameItem[] spawnGround(GameItem[] gameItems, Mesh groundMesh, byte groundLength, byte scale, byte height) {
        for (byte i=0; i < groundLength; i++ ){
            GameItem gameItem = new GameItem(groundMesh);
            gameItem.setScale(scale);
            gameItem.setPosition((scale*i), -4.9f+height, -scale);
            gameItems = insertGameItem(gameItems, gameItem);
        }
        for (byte i=0; i < groundLength; i++ ){
            GameItem gameItem = new GameItem(groundMesh);
            gameItem.setScale(scale);
            gameItem.setPosition((scale*i), -4.9f+height, scale);
            gameItems = insertGameItem(gameItems, gameItem);
        }
        for (byte i=0; i < groundLength; i++ ){
            GameItem gameItem = new GameItem(groundMesh);
            gameItem.setScale(scale);
            gameItem.setPosition((scale*i), -4.9f+height, -2*scale);
            gameItems = insertGameItem(gameItems, gameItem);
        }
        return gameItems;
    }
    public static GameItem[] spawnSea(GameItem[] gameItems, Mesh seaMesh, byte groundLength, byte scale, byte depth) {
        for (byte j = depth; j > -3; j--){
            for (byte i=0; i < groundLength; i++ ){
                GameItem gameItem = new GameItem(seaMesh);
                gameItem.setScale(scale);
                gameItem.setPosition((scale*i), -4.9f, -(j*scale));
                gameItems = insertGameItem(gameItems, gameItem);
            }
        }
        return gameItems;
    }
}
