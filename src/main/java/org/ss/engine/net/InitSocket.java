package org.ss.engine.net;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class InitSocket{
    private DatagramSocket socket;
    private boolean running;
    private InetAddress address;
    private int port;
    private byte[] buf = new byte[256];
    private String currentBuffer;

    public InitSocket() throws SocketException {
        socket = new DatagramSocket(12005);
        System.out.print("waiting for the server...");
    }

    public String[] runSocket() {
        running = true;

        while (running) {
            DatagramPacket packet
                    = new DatagramPacket(buf, buf.length);
            try {
                socket.receive(packet);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (this.address == null){
                address = packet.getAddress();
                port = packet.getPort();
            }

            packet = new DatagramPacket(buf, buf.length, address, port);
            currentBuffer = new String(packet.getData(), 0, packet.getLength());

            if (currentBuffer != null) {
                System.out.print(currentBuffer);
                running = false;
            }

        }
        socket.close();
        return parseAddress();
    }

    public String[] parseAddress(){
        return currentBuffer.split(",");
    }
}
