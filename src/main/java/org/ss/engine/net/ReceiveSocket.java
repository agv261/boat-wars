package org.ss.engine.net;

import org.joml.Vector3f;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Locale;
import java.util.Vector;

public class ReceiveSocket extends Thread{
    private DatagramSocket socket;
    private boolean running;
    private InetAddress address;
    private int port;
    private byte[] buf = new byte[256];
    private String currentBuffer;

    public ReceiveSocket() throws SocketException {
        socket = new DatagramSocket(12005);
    }

    public ReceiveSocket(int port) throws SocketException {
        socket = new DatagramSocket(port);
    }

    public void run() {
        running = true;

        while (running) {
            DatagramPacket packet
                    = new DatagramPacket(buf, buf.length);
            try {
                socket.receive(packet);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (this.address == null){
                address = packet.getAddress();
                port = packet.getPort();
            }

            packet = new DatagramPacket(buf, buf.length, address, port);
            String received
                    = new String(packet.getData(), 0, packet.getLength());
            currentBuffer = received;

            if (received.equals("end")) {
                running = false;
            }
        }
        socket.close();
    }

    public Vector3f parseVelocity() {
        float x;
        float y;
        float z;
        String[] split = currentBuffer.split(",");
        x = -Integer.parseInt(split[0].trim());
        y = Integer.parseInt(split[1].trim());
        z = -Integer.parseInt(split[2].trim());
        return new Vector3f(x,y,z);

    }
    public String getCurrentBuffer(){
        return this.currentBuffer;
    }

    public void clearCurrentBuffer() {
        currentBuffer = null;
    }

    public void stopServer(){ this.running = false;}
}
