package org.ss.engine.net;

import java.io.IOException;
import java.net.*;

public class SendSocket {
    private DatagramSocket socket;
    private InetAddress address;
    private int ptNumber;

    private byte[] buf;

    public SendSocket(String hostname, int ptNumber) throws SocketException, UnknownHostException {
        socket = new DatagramSocket();
        address = InetAddress.getByName(hostname);
        this.ptNumber = ptNumber;
    }

    public void sendEcho(String msg) throws IOException {
        buf = msg.getBytes();
        DatagramPacket packet
                = new DatagramPacket(buf, buf.length, address, ptNumber);
        socket.send(packet);
    }

    public void close() {
        socket.close();
    }
}