package org.ss.game;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector4f;
import static org.lwjgl.opengl.GL11.*;

import org.ss.engine.*;
import org.ss.engine.graph.*;

public class Renderer{

    private static final float FOV = (float) Math.toRadians(60.0f);

    private static final float Z_NEAR = 0.1f;

    private static final float Z_FAR = 1000.0f;

    private static final int MAX_POINT_LIGHTS = 5;

    private static final int MAX_SPOT_LIGHTS = 5;

    private final Transformation transformation;

    private ShaderProgram shaderProgram;

    private ShaderProgram skyboxShaderProgram;

    private final float specularPower;

    public Renderer() {
        transformation = new Transformation();
        specularPower = 10f;
    }

    public void init(Window window) throws Exception {
        // Create shader
        setupSkyboxShader();
        shaderProgram = new ShaderProgram();
        shaderProgram.createVertexShader(Utils.loadResource("/shaders/scene_vertex.vs"));
        shaderProgram.createFragmentShader(Utils.loadResource("/shaders/scene_fragment.fs"));
        shaderProgram.link();

        // Create uniforms for modelView and projection matrices
        shaderProgram.createUniform("projectionMatrix");
        shaderProgram.createUniform("modelViewMatrix");
        shaderProgram.createUniform("texture_sampler");

        // Create uniforms for material
        shaderProgram.createMaterialUniform("material");

        // Create uniform for default color and color flag that controls it
        shaderProgram.createUniform("specularPower");
        shaderProgram.createUniform("ambientLight");
        shaderProgram.createPointLightListUniform("pointLights", MAX_POINT_LIGHTS);
        shaderProgram.createSpotLightListUniform("spotLights", MAX_SPOT_LIGHTS);
        shaderProgram.createDirectionalLightUniform("directionalLight");
    }
    private void setupSkyboxShader() throws Exception {
        skyboxShaderProgram = new ShaderProgram();
        skyboxShaderProgram.createVertexShader(Utils.loadResource("/shaders/skybox_vertex.vs"));
        skyboxShaderProgram.createFragmentShader(Utils.loadResource("/shaders/skybox_fragment.fs"));
        skyboxShaderProgram.link();

        // Create uniforms for projection matrix
        skyboxShaderProgram.createUniform("projectionMatrix");
        skyboxShaderProgram.createUniform("modelViewMatrix");
        skyboxShaderProgram.createUniform("texture_sampler");
        skyboxShaderProgram.createUniform("ambientLight");
    }

//    private void setupSceneShader() throws Exception {
//        // Create shader
//        sceneShaderProgram = new ShaderProgram();
//        sceneShaderProgram.createVertexShader(Utils.loadResource("/shaders/scene_vertex.vs"));
//        sceneShaderProgram.createFragmentShader(Utils.loadResource("/shaders/scene_fragment.fs"));
//        sceneShaderProgram.link();
//
//        // Create uniforms for modelView and projection matrices and texture
//        sceneShaderProgram.createUniform("projectionMatrix");
//        sceneShaderProgram.createUniform("modelViewMatrix");
//        sceneShaderProgram.createUniform("texture_sampler");
//        // Create uniform for material
//        sceneShaderProgram.createMaterialUniform("material");
//        // Create lighting related uniforms
//        sceneShaderProgram.createUniform("specularPower");
//        sceneShaderProgram.createUniform("ambientLight");
//        sceneShaderProgram.createPointLightListUniform("pointLights", MAX_POINT_LIGHTS);
//        sceneShaderProgram.createSpotLightListUniform("spotLights", MAX_SPOT_LIGHTS);
//        sceneShaderProgram.createDirectionalLightUniform("directionalLight");
//    }

    public void clear() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    public void render(Window window, Camera camera, Scene scene) {
        clear();

        if (window.isResized()) {
            glViewport(0, 0, window.getWidth(), window.getHeight());
            window.setResized(false);
        }

        renderSkyBox(window, camera, scene);

        shaderProgram.bind();

        // Update the projection matrix
        Matrix4f projectionMatrix = transformation.getProjectionMatrix(FOV, window.getWidth(), window.getHeight(), Z_NEAR, Z_FAR);
        shaderProgram.setUniform("projectionMatrix", projectionMatrix);

        // Update view Matrix
        Matrix4f viewMatrix = transformation.getViewMatrix(camera);

        // Update Light Uniforms
        renderLights(viewMatrix, scene.getSceneLight());

        shaderProgram.setUniform("texture_sampler", 0);
        // Render each gameItem
        GameItem[] gameItems = scene.getgameItems();
        for (GameItem gameItem : gameItems) {
            if (gameItem != null){
                Mesh mesh = gameItem.getMesh();

                // Set modelView matrix for this item
                Matrix4f modelViewMatrix = transformation.getModelViewMatrix(gameItem, viewMatrix);
                shaderProgram.setUniform("modelViewMatrix", modelViewMatrix);

                // Render the mesh
                shaderProgram.setUniform("material", mesh.getMaterial());
                mesh.render();
            }
        }

        shaderProgram.unbind();
    }
    private void renderSkyBox(Window window, Camera camera, Scene scene) {
        skyboxShaderProgram.bind();

        skyboxShaderProgram.setUniform("texture_sampler", 0);

        Matrix4f projectionMatrix = transformation.getProjectionMatrix();
        skyboxShaderProgram.setUniform("projectionMatrix", projectionMatrix);
        Skybox skybox = scene.getSkybox();
        Matrix4f viewMatrix = transformation.getViewMatrix(camera);
        viewMatrix.m30(0);
        viewMatrix.m31(0);
        viewMatrix.m32(0);
        Matrix4f modelViewMatrix = transformation.buildModelViewMatrix(skybox, viewMatrix);
        skyboxShaderProgram.setUniform("modelViewMatrix", modelViewMatrix);
        skyboxShaderProgram.setUniform("ambientLight", scene.getSceneLight().getAmbientLight());

        scene.getSkybox().getMesh().render();

        skyboxShaderProgram.unbind();
    }

    private void renderLights(Matrix4f viewMatrix, SceneLight sceneLight) {

        shaderProgram.setUniform("ambientLight", sceneLight.getAmbientLight());
        shaderProgram.setUniform("specularPower", specularPower);

        // Process Point Lights
        PointLight[] pointLightList = sceneLight.getPointLightList();
        int numLights = pointLightList != null ? pointLightList.length : 0;
        for (int i = 0; i < numLights; i++) {
            // Get a copy of the point light object and transform its position to view coordinates
            PointLight currPointLight = new PointLight(pointLightList[i]);
            Vector3f lightPos = currPointLight.getPosition();
            Vector4f aux = new Vector4f(lightPos, 1);
            aux.mul(viewMatrix);
            lightPos.x = aux.x;
            lightPos.y = aux.y;
            lightPos.z = aux.z;
            shaderProgram.setUniform("pointLights", currPointLight, i);
        }

        // Process Spot Ligths
        SpotLight[] spotLightList = sceneLight.getSpotLightList();
        numLights = spotLightList != null ? spotLightList.length : 0;
        for (int i = 0; i < numLights; i++) {
            // Get a copy of the spot light object and transform its position and cone direction to view coordinates
            SpotLight currSpotLight = new SpotLight(spotLightList[i]);
            Vector4f dir = new Vector4f(currSpotLight.getConeDirection(), 0);
            dir.mul(viewMatrix);
            currSpotLight.setConeDirection(new Vector3f(dir.x, dir.y, dir.z));

            Vector3f lightPos = currSpotLight.getPointLight().getPosition();
            Vector4f aux = new Vector4f(lightPos, 1);
            aux.mul(viewMatrix);
            lightPos.x = aux.x;
            lightPos.y = aux.y;
            lightPos.z = aux.z;

            shaderProgram.setUniform("spotLights", currSpotLight, i);
        }

        // Get a copy of the directional light object and transform its position to view coordinates
        DirectionalLight currDirLight = new DirectionalLight(sceneLight.getDirectionalLight());
        Vector4f dir = new Vector4f(currDirLight.getDirection(), 0);
        dir.mul(viewMatrix);
        currDirLight.setDirection(new Vector3f(dir.x, dir.y, dir.z));
        shaderProgram.setUniform("directionalLight", currDirLight);

    }

    public void cleanup() {
        if (shaderProgram != null) {
            shaderProgram.cleanup();
        }
    }
}
