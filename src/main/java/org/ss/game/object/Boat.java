package org.ss.game.object;
import org.ss.engine.GameItem;
import org.ss.engine.graph.Mesh;

import javax.swing.*;

public class Boat extends GameItem{
    public String state;

    private int health;

    public Boat(Mesh mesh){
        super(mesh);
        this.state = "Floating";
        health = 100;
    }

    public int handleCollision(){
        this.health -= 25;
        return this.health;
    }

}

