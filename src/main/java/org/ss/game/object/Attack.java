package org.ss.game.object;

import org.ss.engine.GameItem;

public class Attack extends Animation{

    private final float range;

    private final byte height;

    private final byte strength;

    private final int dangerStart;

    private final int dangerEnd;

    public String result;

    private GameItem target;

    public Attack(String path2Attack, float range, byte height, int dangerStart, int dangerEnd, String result) throws Exception {
        super(path2Attack, false,false);
        this.range = range;
        this.height = height;
        this.dangerStart = dangerStart;
        this.dangerEnd = dangerEnd;
        this.strength = 1;
        this.result = result;
    }

    public boolean canHit(){
        return (this.getFrame() > dangerStart && this.getFrame() < dangerEnd);
    }

    public String detectCollision(float distance, boolean reversed){
        if (distance <= this.range) return this.result;

        else  return "MISS";
    }
}