package org.ss.game.object;
import org.ss.engine.GameItem;
import org.ss.engine.graph.Mesh;

public class Player extends GameItem {
    public String state;

    private float radians;

    private int health;

    private int actionCounter;

    public Player(Mesh mesh){
        super(mesh);
        this.state = "Floating";
        this.actionCounter = 999;
        this.health = 100;
    }

    public void move(){
        this.actionCounter -=9;
    }

    public void shoot(){
        this.actionCounter -= 100;
    }

    public void restore(){
        this.actionCounter = 999;
    }

    public void handleCollision(){
        this.health -= 25;

    }
    public int getActionCounter() {
        return actionCounter;
    }

    public int getHealth(){
        return this.health;
    }
}

