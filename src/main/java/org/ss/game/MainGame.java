package org.ss.game;

import org.joml.Vector2f;
import org.joml.Vector3f;
import static org.lwjgl.glfw.GLFW.*;

import org.joml.Vector4f;
import org.ss.engine.*;
import org.ss.engine.Window;
import org.ss.engine.graph.*;
import org.ss.engine.net.InitSocket;
import org.ss.engine.net.ReceiveSocket;
import org.ss.engine.net.SendSocket;
import org.ss.game.object.Boat;
import org.ss.game.object.Player;

import java.io.IOException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.List;

public class MainGame implements IGameLogic {

    private static final float MOUSE_SENSITIVITY = 0.2f;

    private final Vector3f cameraVelocity;

    private final Vector3f playerVelocity;

    private final Renderer renderer;

    private final Camera camera;

    private GameItem[] gameItems;

    private org.ss.game.object.Player Player;

    private Boat enemy;

    private GameItem cannonBall;

    private List<int[]> coords;

    private final SendSocket client;

    private final ReceiveSocket server;

    private Scene scene;

    private float lightAngle;

    private static final float CAMERA_POS_STEP = 0.05f;

    private String state;

    public MainGame() throws SocketException, UnknownHostException {
        String[] args = new InitSocket().runSocket();
        renderer = new Renderer();
        camera = new Camera();
        camera.setPosition(40f, 3f, 10f);
        cameraVelocity = new Vector3f(0.0f, 0.0f, 0.0f);
        playerVelocity = new Vector3f(0.0f, 0.0f, 0.0f);
        lightAngle = -90;

        client = new SendSocket(args[0],Integer.parseInt(args[1]));
        int listenPort = Integer.parseInt(args[2].trim());
        server = new ReceiveSocket(listenPort);
        server.start();
        this.state = args[3];


    }

    public MainGame(String[] args) throws SocketException, UnknownHostException {
        renderer = new Renderer();
        camera = new Camera();
        camera.setPosition(47f, 3f, 0f);
        cameraVelocity = new Vector3f(0.0f, 0.0f, 0.0f);
        playerVelocity = new Vector3f(0.0f, 0.0f, 0.0f);
        lightAngle = -90;

        client = new SendSocket(args[0].trim(),Integer.parseInt(args[1].trim()));
        server = new ReceiveSocket(Integer.parseInt(args[2].trim()));
        server.start();
    }

    @Override
    public void init(Window window) throws Exception {
        renderer.init(window);
        scene = new Scene();
        coords = scene.getObstaclesCoords();


        float reflectance = 1f;
        Mesh cubeMesh = OBJLoader.loadMesh("/models/cube.obj");
        Texture cubeTexture = new Texture(System.getProperty("user.dir")+"\\textures\\water.png");
        Material cubeMaterial = new Material(cubeTexture, reflectance);
        cubeMesh.setMaterial(cubeMaterial);

        Mesh playerMesh = OBJLoader.loadMesh("/models/Ship001.obj");
        Material playerMaterial = new Material(new Vector4f(1.0f, 1.0f, 1.0f, 1.0f), reflectance);
        playerMesh.setMaterial(playerMaterial);

        Mesh obstacleMesh = OBJLoader.loadMesh("/models/obstacle.obj");
        Texture obsTexture = new Texture(System.getProperty("user.dir")+"\\textures\\grassblock.png");
        Material obsMaterial = new Material(obsTexture, reflectance);
        obstacleMesh.setMaterial(obsMaterial);

        Mesh cannonBallMesh = OBJLoader.loadMesh("/models/cannon-ball.obj");
        Material cannonBallMat = new Material(new Vector4f(0.2f,0.2f,0.2f,1.0f), 1f);
        cannonBallMesh.setMaterial(cannonBallMat);

        gameItems = new GameItem[]{};
        Player = new Player(playerMesh);
        Player.setPosition(47,0,0);

        enemy = new Boat(playerMesh);
        enemy.setPosition(47,3,-70);

        cannonBall = new GameItem(cannonBallMesh);

        gameItems = GameItemLoader.insertGameItem(this.gameItems, enemy);
        gameItems = GameItemLoader.spawnSea(gameItems, cubeMesh, (byte) 20, (byte) 5, (byte) 14);
        gameItems = GameItemLoader.spawnObstacles(gameItems, obstacleMesh, scene.getObstaclesCoords());

        scene.setGameItems(gameItems);

        Skybox skybox = new Skybox(OBJLoader.loadMesh("/models/cube.obj"), System.getProperty("user.dir")+"/textures/sea-horizon.png");
        skybox.setScale(50f);
        scene.setSkybox(skybox);

        // Setup Lights
        setupLights();
    }
    private void setupLights() {
        SceneLight sceneLight = new SceneLight();
        scene.setSceneLight(sceneLight);

        // Ambient Light
        sceneLight.setAmbientLight(new Vector3f(1.0f, 1.0f, 1.0f));

        // Directional Light
        float lightIntensity = 0.25f;
        Vector3f lightPosition = new Vector3f(-1, 0, 0);
        sceneLight.setDirectionalLight(new DirectionalLight(new Vector3f(1, 1, 1), lightPosition, lightIntensity));
    }

    @Override
    public void input(Window window, MouseInput mouseInput) throws IOException {
        cameraVelocity.set(0,0,0);
        if (this.Player.getActionCounter() < 1){
            this.state = "ENEMY_TURN";
            endTurn();
        }
        switch (state){
            case "GAME":
                float x = camera.getPosition().x;
                float z = camera.getPosition().z;

                // Change camera position keys
                if (window.isKeyPressed(GLFW_KEY_W)) {
                    if (obstacleCollision(x, z) || boundCollision(x, z, false)) {
                        cameraVelocity.z = 10;
                    } else {
                        cameraVelocity.z = -5;
                        send();
                        Player.move();
                    }


                } else if (window.isKeyPressed(GLFW_KEY_S)){
                    if (obstacleCollision(x, z) || boundCollision(x, z, false)){
                        cameraVelocity.z = -10;
                    } else {
                        cameraVelocity.z = 5;
                        send();
                        Player.move();
                    }

                }

                if (window.isKeyPressed(GLFW_KEY_A)) {
                    if (obstacleCollision(x, z) || boundCollision(x, z, false)){
                        cameraVelocity.x = 10;
                    } else {
                        cameraVelocity.x = -5;
                        send();
                        Player.move();
                    }


                } else if (window.isKeyPressed(GLFW_KEY_D)) {
                    if (obstacleCollision(x, z) || boundCollision(x, z, false)){
                        cameraVelocity.x = -10;
                    } else {
                        cameraVelocity.x = 5;
                        send();
                        Player.move();
                    }


                }
                if (window.isKeyPressed(GLFW_KEY_SPACE)){
                    state = "ENEMY_TURN";
                    endTurn();
                    System.out.print("Enemy's turn!");
                    cameraVelocity.set(0,0,0);

                }
                if (window.isKeyPressed(GLFW_KEY_P) && Player.state.equals("Floating")){
                    System.out.print(camera.getRotation());
                    Player.state = "Shooting";
                    System.out.print("Player shot the cannon");
                    shoot(mouseInput);
                }
                if (Player.state.equals("Shooting")){
                    x = cannonBall.getPosition().x;
                    z = cannonBall.getPosition().z;
                    if ( obstacleCollision(x, z) || boundCollision(x, z, true) ){
                        System.out.print("Collision!");
                        Player.state = "Floating";
                    } else {
                        enemyHit(x,z);
                    }
                }
                break;


            case "ENEMY_TURN":
                enemy.setVelocity(0,0,0);
                if (window.isKeyPressed(GLFW_KEY_ESCAPE)) {
                    state = "GAME";
                }

                String received = server.getCurrentBuffer();
                if (received != null){
                    Vector3f enemy_pos = server.parseVelocity();
                    server.clearCurrentBuffer();
                    if (enemy_pos.z > 100 || enemy_pos.z < -100 ){
                        enemy_pos.z = (float) Math.floor(enemy_pos.z/100);
                    } else if (enemy_pos.z > 10 || enemy_pos.z < -10 ){
                        enemy_pos.z = (float) Math.floor(enemy_pos.z/10);
                    }
                    enemy.setVelocity(enemy_pos.x, 0, enemy_pos.z);

                    if (enemy_pos.y == -1){
                        state = "GAME";
                        Player.restore();
                        System.out.print("Your Turn!"+"\n");

                    } else if (enemy_pos.y < -24){
                        Player.handleCollision();
                        if (Player.getHealth() == 0){
                            sendLoss();
                        }

                    } else if (enemy_pos.y > 887){
                        System.out.print("You lost, better luck next time :(");
                        System.exit(-1);
                    }
                }
                break;
        }

    }
    public boolean obstacleCollision(float x, float z){
        int[] xs = coords.get(0);
        int[] zs = coords.get(1);
        int[] scales = coords.get(2);
        for (int i=0; i < xs.length; i++){
            if (x > xs[i]-scales[i]/2 && x < xs[i]+scales[i]/2 &&
                    z > zs[i]-scales[i]/2 && z < zs[i]+scales[i]/2)
                return true;
        }
        return false;
    }

    public boolean boundCollision(float x, float z, boolean entire){
        if (entire){
            if ((x > 12 && x <= 82) &&
                    (z > -70 && z <= 0)){
                return false;
            } else{
                System.out.print("\n"+"Bound collision!");
                return true;
            }

        } else {
            return (!(x > 12) || !(x <= 82)) ||
                    (!(z > -35) || !(z <= 0));
        }

    }

    public void enemyHit(float x, float z) throws IOException {
        float posx = enemy.getPosition().x;
        float posz = enemy.getPosition().z;
        if ((x > posx-1 && x <= posx+1) &&
                (z > posz-1 && z <= posz+1)){
            System.out.print("enemy got hit!");
            int remaining = enemy.handleCollision();
            if (remaining == 0){
                sendLoss();
            } else {
                sendHit();
            }
        }

    }
    private void shoot(MouseInput mouseInput){
        Player.shoot();
        System.out.print(camera.getRotation() + "\n");
        double angle = camera.getRotation().y;
        double v = Math.sin(angle - Math.PI/2);
        float dz = (float) (v * 10f);
        System.out.print(dz + "\n");

        double w = Math.cos(angle - Math.PI/2);
        float dx = (float) (w * 10f);
        System.out.print(dx + "\n");


        gameItems = GameItemLoader.insertGameItem(gameItems, cannonBall);
        scene.setGameItems(gameItems);
        cannonBall.setScale(1f);
        Vector3f originalPos = new Vector3f();
        originalPos.z = camera.getPosition().z;
        originalPos.x = camera.getPosition().x;
        originalPos.y = camera.getPosition().y;
        cannonBall.setPosition(originalPos);
        cannonBall.setVelocity(dx,0,dz);
    }

    private void send() throws IOException {
        String msg = String.valueOf((int) cameraVelocity.x)
                + ',' + (int) cameraVelocity.y
                + ',' + (int) cameraVelocity.z;
        client.sendEcho(msg);

    }

    private void sendHit() throws IOException {
        client.sendEcho(String.valueOf((int) cameraVelocity.x)
                + ',' + -25
                + ',' + (int) cameraVelocity.z);
    }

    private void sendLoss() throws IOException {
        client.sendEcho(String.valueOf((int) camera.getPosition().x)
                + ',' + 888
                + ',' + camera.getPosition().z);
    }

    private void endTurn() throws IOException{
        client.sendEcho("0,-1,0");
    }

    @Override
    public void update(float interval, MouseInput mouseInput) {
        camera.movePosition(cameraVelocity.x * CAMERA_POS_STEP, cameraVelocity.y * CAMERA_POS_STEP, cameraVelocity.z * CAMERA_POS_STEP);

        Player.movePosition(playerVelocity.x * CAMERA_POS_STEP, playerVelocity.y * CAMERA_POS_STEP, playerVelocity.z * CAMERA_POS_STEP);

        enemy.movePosition(enemy.getVelocity().x * CAMERA_POS_STEP , enemy.getVelocity().y * CAMERA_POS_STEP,enemy.getVelocity().z * CAMERA_POS_STEP *.9f);

        cannonBall.movePosition(cannonBall.getVelocity().x * CAMERA_POS_STEP, cannonBall.getVelocity().y * CAMERA_POS_STEP, cannonBall.getVelocity().z * CAMERA_POS_STEP);

        if (mouseInput.isRightButtonPressed()) {
            Vector2f rotVec = mouseInput.getDisplVec();
            camera.moveRotation(0, rotVec.y * MOUSE_SENSITIVITY, 0);
        }

        SceneLight sceneLight = scene.getSceneLight();

        // Update directional light direction, intensity and colour
        DirectionalLight directionalLight = sceneLight.getDirectionalLight();
        lightAngle += 1.1f;
        if (lightAngle > 90) {
            directionalLight.setIntensity(0);
            if (lightAngle >= 360) {
                lightAngle = -90;
            }
            sceneLight.getAmbientLight().set(0.3f, 0.3f, 0.4f);
        } else if (lightAngle <= -80 || lightAngle >= 80) {
            float factor = 1 - (Math.abs(lightAngle) - 80) / 10.0f;
            sceneLight.getAmbientLight().set(factor, factor, factor);
            directionalLight.setIntensity(factor);
            directionalLight.getColor().y = Math.max(factor, 0.9f);
            directionalLight.getColor().z = Math.max(factor, 0.5f);
        } else {
            sceneLight.getAmbientLight().set(1, 1, 1);
            directionalLight.setIntensity(1);
            directionalLight.getColor().x = 1;
            directionalLight.getColor().y = 1;
            directionalLight.getColor().z = 1;
        }
        double angRad = Math.toRadians(lightAngle);
        directionalLight.getDirection().x = (float) Math.sin(angRad);
        directionalLight.getDirection().y = (float) Math.cos(angRad);
    }

    @Override
    public void render(Window window) {
        renderer.render(window, camera, scene);
    }

    public void stopNetwork() throws InterruptedException {
        client.close();
        server.join();
        server.stopServer();
    }

    @Override
    public void cleanup() {
        renderer.cleanup();
        for (GameItem gameItem : gameItems) {
            gameItem.getMesh().cleanUp();
        }
    }
}
