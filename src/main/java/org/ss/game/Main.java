package org.ss.game;

import org.lwjgl.system.CallbackI;
import org.ss.engine.GameEngine;
import org.ss.engine.IGameLogic;

public class Main {

    public static void main(String[] args) {
        try {
            boolean vSync = true;
            int WIDTH = 700;
            int HEIGHT = 600;
            IGameLogic gameLogic = new MainGame();
            GameEngine gameEng = new GameEngine("GAME", WIDTH, HEIGHT, vSync, gameLogic);
            gameEng.start();
        } catch (Exception excp) {
            System.exit(-1);
        }
    }
}
