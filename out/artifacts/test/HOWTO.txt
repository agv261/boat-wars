!!! You may need to surround your inputs with "" !!!

Method 1: call the test_game.sh in a command line to start sending updates on "localhost":12006 and start listening on port 12010

Method 2: call start_game.sh and fill in the values to start the game with the ip, sending port and listening port you've listed.

Method 3: open a command line and type:
	java -jar test.jar ip sendPort listenPort

to start the game and send updates to ip:sendPort and receive updates on listenPort.  